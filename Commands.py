#-------------------------------------------------------------------------------
# Author(s):
#    Simone Alioli, Frank Tackmann
#
# Copyright:
#    Copyright (C) 2017, 2018 Geneva Collaboration
#
#    This file is part of the Geneva MC framework. Geneva is distributed under
#    the terms of the GNU General Public License version 3 (GPLv3), see the
#    COPYING file that comes with this distribution for details.
#    Please respect the academic usage guidelines in the GUIDELINES file.
#
# Description:
#    Definition of class FixedOrder
#-------------------------------------------------------------------------------
import sys
import argparse

from utils import format_tag, join_paths, get_filenames, format_path


"""Generate the commands to run a fixed-order calculation.

Returns a list of all commands required to run a given stage in the form

   [[name1, executable1, [arguments1a, arguments1b, ...], [logfile1a, logfile1b, ...]],
    [name2, executable2, [arguments2a, arguments2b, ...], [logfile1a, logfile1b, ...]],
    ...
   ]

where
   name       name of the substage
   executable command to be executed
   arguments  the arguments that each command must be run with
   logfile    the log file used for each command when the command list is
              executed via the geneva <stage> machinery

This implies that the following commands need to be run
   executable1 arguments1a
   executable1 arguments1b
   ...

   executable2 arguments2a
   executable2 arguments2b
   ...

   ...

All commands of the same substage (same name) can be run in parallel, while
the different substages must be run sequentially.
"""

""" Define all arguments
"""
def parse_args(command_args):

   """Defines and parses the options of geneva
   """
   parser = argparse.ArgumentParser(description = "Creating the list of commands")

   parser.add_argument("--text1", help = "First argument")
   parser.add_argument("--text2", help = "First argument")
   
   return parser.parse_args(command_args.split())
#=======================================
#              SETUP STAGE
#=======================================
def getCommands(binpath, outpath, num_runs, command_args):

   binPath = format_path(binpath)
   outPath = format_path(outpath)
   logPath = join_paths(outpath, "log")

   commandList = []

   name_1 = "Executable_1"
   executable_1 = format_path(binPath) + "code_1"
   commonOptions_1 = ""
   optionList = []
   logFileList = []
   for i in range(num_runs):
      # Create the grids for each iteration.
      optionList.append(commonOptions_1 + " --arg " + str(i) + " --text " + command_args.text1)
      logFileList.append(logPath + "logFile_" + name_1 +"_" + str(i) + ".log")
   commandList.append([name_1, executable_1, optionList, logFileList])

   name_1 = "Executable_2"
   executable_1 = format_path(binPath) + "code_2"
   commonOptions_1 = ""
   optionList = []
   logFileList = []
   for i in range(num_runs):
      # Create the grids for each iteration.
      optionList.append(commonOptions_1 + " --arg " + str(i) + " --text " + command_args.text2)
      logFileList.append(logPath + "logFile_" + name_1 +"_" + str(i) + ".log")
   commandList.append([name_1, executable_1, optionList, logFileList])

   return commandList

def getAllCommands(args):
   """
   Get the list of commands
   """
   # Arguments with '-' in name are converted to '_'
   try:
      command_args = parse_args(args.command_args)
   except:
      sys.exit("[ERROR] The provided command_args do not match what is required by the command_provider " + args.command_provider + ". Please double check your input")
   return getCommands(args.binpath, args.outpath, args.num_runs, command_args)
