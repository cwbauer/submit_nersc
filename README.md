# README #

Submit_NERSC contains code that allows to run general code in parallel on the NERSC 
architecture.

### General Idea ###

Uses mpi4py this code to farm individual runs onto the different cores of the NERSC
architecture. 

The code can have several stages, each consisting of an executable with a set of options

Stages are executed sequentially, while the different options of the executable are 
run in parallel. Each run will have its own output logfile.

Currently supports Cori-Haswell and Cori-KNL


### Setup ###

Simply download the code and run from that directory.

### Usage ###

To tell the scripts what code to run is to edit the file 

*commands.py*

which specifies the stages and commands to run through a list in the following form

    [[name1, executable1, [arguments1a, arguments1b, ...], [logfile1a, logfile1b, ...]],
     [name2, executable2, [arguments2a, arguments2b, ...], [logfile1a, logfile1b, ...]],
     ...
    ]

where
    
    name       name of the stage
    executable command to be executed at given stage
    arguments  the arguments that each command must be run with
    logfile    the log file used for each command

This implies that the following commands need to be run
    
    executable1 arguments1a
    executable1 arguments1b
    ...
    executable2 arguments2a
    executable2 arguments2b
    ...
    ...

### Getting help ###

email cwbauer@lbl.gov
